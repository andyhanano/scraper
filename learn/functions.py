# -*- coding: utf-8 -*-

from pymongo import MongoClient


def getClient() :
    connect = MongoClient('ec2-52-198-180-103.ap-northeast-1.compute.amazonaws.com', 27017)
    connect.hose_race.authenticate('root', 'pass')
    return connect.hose_race

def lean(hose):
    score = 0;
    #corseScore = sameCorse(hose)
    longScore = sameLong(hose)
    longWeigth = sameWeight(hose)
    #score += corseScore
    score += longScore
    score += longWeigth
    print hose['race_name'].encode('utf-8') + ':' +hose['hose_name'].encode('utf-8')
    #print ' : 同一コース:' + str(corseScore)
    print ' : 同一距離:' + str(longScore)
    print ' : 同一斤量:' + str(longWeigth)
    hose['score'] = score
    db = getClient()
    collect = db.card_data
    collect.save(hose)

def getRaces(json):
    db = getClient()
    collect = db.hoses
    results = collect.find(json).sort('race_date', -1)
    return results

def getRaceWeaight(race_id):
    db = getClient()
    collect = db.races
    results = collect.find({'race_id': race_id})
    if 'score_weight' in results[0] :
        return results[0]['score_weight']
    else :
        return 0

def daysWeaight(days):
    if days < 90:
        return 1
    elif 90 <= days and days < 180:
        return 0.6
    elif 180 <= days and days < 270:
        return 0.4
    else:
        return 0.2

# 使わなくする
def sameCorse(hose):
    hose_name = hose['hose_name']
    results = getRaces({'hose_name': hose_name, 'race_corse': hose['race_corse']})
    corse_order = 200
    corse_score = 0
    for item in results:
        order = item['order']
        if order.isdigit():
            corse_score += corse_order * getRaceWeaight(item['race_id'])/ int(order)
    return corse_score

def sameLong(hose):
    hose_name = hose['hose_name']
    results = getRaces({'$and' :[{'hose_name': hose_name},{'corse_data': {'$gt': int(hose['corse_data']) - 400, '$lt': int(hose['corse_data']) + 400}}]})
    long_order = 300
    long_score = 0
    corse_data = hose['corse_data']
    for item in results:
        order = item['order']
        race_corse = item['race_corse']
        long = item['corse_data']
        date = item['race_date']
        dateDiff = hose['race_date'] - date
        days = dateDiff.days;
        if 400 < days:
            break
        if order.isdigit():
            addPar = 0
            if race_corse == hose['race_corse']:
                addPar = 0.2
            checkOrder = long_order * (getRaceWeaight(item['race_id']) +  daysWeaight(days))
            if long <  corse_data - 200:
                checkOrder *= (0.4 + addPar)
            elif long < corse_data:
                checkOrder *= (0.6 + addPar)
            elif long == corse_data :
                checkOrder *= (1 + addPar)
            elif corse_data < long and long <= corse_data + 200:
                checkOrder *= (0.6 + addPar)
            elif corse_data + 200 < long:
                checkOrder *= (0.4 + addPar)
            long_score += checkOrder / int(order)
    return long_score

def sameWeight(hose):
    hose_name = hose['hose_name']
    results = getRaces({'hose_name': hose_name, 'basis_weight': hose['basis_weight']})

    long_order = 50
    long_score = 0
    for item in results:
        order = item['order']
        date = item['race_date']
        dateDiff = hose['race_date'] - date
        days = dateDiff.days;
        if order.isdigit():
            long_score += long_order * (getRaceWeaight(item['race_id']) +  daysWeaight(days)) / int(order)
    return long_score