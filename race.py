# coding: UTF-8
import datetime
class Race:
    def __init__(self, name, race_info, num , place, date, url):
        self.race_name = name
        self.race_info = race_info
        self.race_number = num
        self.race_corse = place
        newDate = datetime.datetime.strptime(date.encode('utf-8'),'%Y/%m/%d')
        self.race_date = newDate
        self.url = url
        self.results = [];
        self.headers = {'order':'着順'
            ,'gate_num':'枠番'
            ,'hose_num':'馬番'
            ,'hose_name':'馬名'
            ,'hose_age':'性齢'
            ,'basis_weight':'斤量'
            ,'jockey':'騎手'
            ,'time':'タイム'
            ,'incompatibility':'着差'
            ,'shissu':'ﾀｲﾑ指数'
            ,'through':'通過'
            ,'last_rap':'上り'
            ,'odds':'単勝'
            ,'fav':'人気'
            ,'weight':'馬体重'
            ,'traning_time':'調教ﾀｲﾑ'
            ,'comment':'厩舎ｺﾒﾝﾄ'
            ,'sub':'備考'
            ,'trainer':'調教師'
            ,'owner':'馬主'
            ,'prize':'賞金(万円)'}

    
    def addResult(self, result):
        self.results.append(result)
        return self
    def printResult(self ):
        for idx in range(len(self.results)):
            result = self.results[idx]
            for header in self.headers:
                header = unicode(header, 'utf-8')
                val = unicode(result.get(header, '-'), 'utf-8')
                print header + ":" + val
        return self
    def dump(self, ):
        result = {
            'race_id':self.race_date.strftime('%Y/%m/%d') + self.race_corse + self.race_number,
            'race_name':self.race_name,
            'race_info':self.race_info,
            'race_number':self.race_number,
            'race_corse':self.race_corse,
            'race_date':self.race_date,
            'str_race_date':self.race_date.strftime('%Y/%m/%d'),
            'url':self.url,
        }
        return result

    def getHoseData(self,):
        result = []
        if len(self.results) != 0:
            for race_result in self.results:
                data = {
                    'race_id' : self.race_date + self.race_corse + self.race_number,
                    'race_name':self.race_name,
                    'race_info':self.race_info,
                    'race_number':self.race_number,
                    'race_corse':self.race_corse,
                    'race_date':self.race_date,
                }
                for key, value in self.headers.items():
                    if unicode(value, 'utf-8') in race_result.keys():
                        data[key] = race_result[unicode(value,'utf-8')]
                    else:
                        pass
                result.append(data)
        return result


