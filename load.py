# coding: UTF-8

import datetime
import multiprocessing
import re
from multiprocessing import Pool
from pymongo import MongoClient
from conf import mongo
from loader import PageParser
from race import Race

headers = ['着順','枠番','馬番','馬名','性齢','斤量','騎手','タイム','着差','ﾀｲﾑ指数','通過','上り','単勝','人気','馬体重','調教ﾀｲﾑ','厩舎ｺﾒﾝﾄ','備考','調教師','馬主','賞金(万円)']

db = mongo.getMongoClient()
db.drop_collection('races')
db.drop_collection('hoses')
db.drop_collection('dates')
db.drop_collection('errors')
connect.close()

def dateLoad(date):
    parser = PageParser()
    url = 'http://db.netkeiba.com/race/list/' + date + '/'
    html = parser.parse(url)
    for a in parser.parsed.find_all(href=re.compile("^/race/[0-9]+/")):
        #dic[date].append(a.get('href'))
        collect = db.dates
        collect.insert({'url': a.get('href'), 'date': date})

def createLoadData():
    if db.dates.count() != 0:
        return db.dates.find()
    list = []
    for date in range(7300):
        target = datetime.date.today() + datetime.timedelta(days=(-1 * date))
        list.append(target.strftime("%Y%m%d"))
    pool = Pool(20)
    pool.map(dateLoad, list)
    return db.dates.find()
def logic(_paths):
     try :
        path = _paths["url"]
        parser = PageParser()
        url = 'http://db.netkeiba.com' + path
        html = parser.parse(url)
        if html == False:
            db = mongo.getMongoClient()
            errors = db.errors
            errors.insert({'url': path})
        if html != False and len(parser.getParsed().select(".race_place a.active")) != 0:
            corse = parser.getParsed().select(".race_place a.active")[0].string
            race_num = parser.getParsed().select(".race_num a.active")[0].string
            race_info = parser.getParsed().select(".racedata diary_snap_cut span")[0].string
            race_name = parser.getParsed().title.string.split(unicode('｜', 'utf-8'))[0].strip()
            date = parser.getParsed().title.string.split(unicode('｜', 'utf-8'))[1].strip()
            date = date.split(unicode('|', 'utf-8'))[0].strip()
            race = Race(race_name, race_info, race_num, corse, date, url)
            trs = parser.getParsed().select(".race_table_01 tr")
            for tr in trs:
                idx = 0
                race_result ={}
                for result in tr.select('td'):
                    header = unicode(headers[idx], 'utf-8')
                    idx += 1
                    if len(result.select('a')) != 0:
                        if len(result.select('a img')) != 0:
                            race_result[header] = result.select('a')[0].get("href").encode('utf-8')
                        if isinstance(result.select('a')[0].string,type(None)) != True:
                            race_result[header] = result.select('a')[0].string.encode('utf-8')
                    else:
                        if isinstance(result.string,type(None)) != True:
                            race_result[header] = result.string.encode('utf-8')
                if len(tr.select('td')) != 0:
                    race = race.addResult(race_result)
            json_data = race.dump()
            if len(json_data) != 0:
                db = mongo.getMongoClient()
                collect = db.races
                collect.insert(json_data)
                collect = db.hoses
                hoses = race.getHoseData()
                if hoses != None and len(hoses) != 0:
                    for hose in hoses:
                        collect.insert(hose)
                connect.close()

     except Exception as e:
        print '=== エラー内容 ==='
        print 'type:' + str(type(e))
        print 'args:' + str(e.args)
        print 'message:' + e.message
        print 'e自身:' + str(e)
     finally:
         pass
def load():
    dic = createLoadData()
    p = Pool(multiprocessing.cpu_count())
    p.map(logic, dic)
    db = mongo.getMongoClient()
    errors = db.errors.find()
    while db.errors.count() != 0:
        db.drop_collection('errors')
        p = Pool(multiprocessing.cpu_count())
        p.map(logic, errors)
        errors = db.errors.find()

if __name__ == "__main__":
    load()
    print '正常終了'
