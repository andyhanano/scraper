# coding: UTF-8
import datetime
import multiprocessing
import re
from multiprocessing import Pool

from conf import mongo
from loader import PageParser
from race import Race
import traceback

baseUrl = 'http://race.netkeiba.com/?pid=race_list_sub&id=c'
exec_date = datetime.datetime.now()
db = mongo.getMongoClient()
db.drop_collection("card_fav_urls")
def loadCard(target):
    parser = PageParser()
    url = baseUrl + target
    print url
    html = parser.parse(url)
    for a in parser.parsed.find_all(href=re.compile("^/\?pid")):
        pattern = re.compile("([0-9]+)$")
        matchOB = pattern.search(a.get('href'))
        if matchOB:
            fav_url = "http://race.netkeiba.com/?pid=yoso&id=c" + matchOB.group()  # 'ca'
            collect = db.card_fav_urls
            if collect.find({'url': fav_url}).count() == 0:
                collect.insert({'url': fav_url, 'date': target})
    return db.card_fav_urls.find({'date':target})

##
# 出馬表のパースロジック
#
def parseLogic(data):
    try :
        url = data["url"]
        parser = PageParser()
        html = parser.parse(url)
        #print html
        if html != False and len(parser.getParsed().select(".race_place a.active")) != 0:
            corse = parser.getParsed().select(".race_place a.active")[0].string
            if len(parser.getParsed().select(".race_num a.active")) == 0:
                db = mongo.getMongoClient()
                db.errors.insert(url)
                return
            race_place = parser.getParsed().select(".race_place .fc .active")
            print race_place[0].string
            race_num = parser.getParsed().select(".race_num .fc .active")
            print race_num[0].string
            race_num = parser.getParsed().select(".race_num a.active")[0].string
            race_info = parser.getParsed().select(".racedata.fc span")[0].string
            #print race_info
            race_name = parser.getParsed().title.string

            # date = parser.getParsed().title.string.split(unicode(' ', 'utf-8'))[0].strip()
            # date = date.split(unicode('|', 'utf-8'))[0].strip()
            # race = Race(race_name, race_info, race_num, corse, date, 'http://race.netkeiba.com' + url["url"])
            # print race.dump()
        # for tr in parser.getParsed().select(".race_table_01 tr"):
        #     for umaban in tr.select(".umaban"):
        #         print umaban.string
        #     for shirushi in tr.select(".shirushi_cell"):
        #         print shirushi.string


    except Exception as e:
        print '=== エラー内容 ==='
        print 'type:' + str(type(e))
        print 'args:' + str(e.args)
        print 'message:' + e.message
        print 'e自身:' + str(e)
        traceback.print_exc()
    finally:
        pass

def leaning():
    db = mongo.getMongoClient()
    collect = db.card_data
    p = Pool(multiprocessing.cpu_count())
    p.map(functions.lean, collect.find())

def load():
    # target = datetime.datetime.now()
    # if 17 <= int(target.strftime("%H")):
    #     target = datetime.date.today() + datetime.timedelta(days=(1))
    # else:
    #     target = datetime.date.today()
    # dic = loadCard(target.strftime("%m%d"))
    dic = loadCard('0204')
    p = Pool(multiprocessing.cpu_count())
    p.map(parseLogic, dic)
    # dic = loadCard('1225')
    # p = Pool(multiprocessing.cpu_count())
    # p.map(parseLogic, dic)
    db = mongo.getMongoClient()
    errors = db.errors.find()
    while db.errors.count() != 0:
        db.drop_collection('errors')
        p = Pool(multiprocessing.cpu_count())
        p.map(parseLogic, errors)
        errors = db.errors.find()
if __name__ == "__main__":
    load()
    # updateRaces()
    # leaning()
    #print '正常終了'
