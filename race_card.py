# coding: UTF-8
import datetime
import multiprocessing
import re
from multiprocessing import Pool
from pymongo import MongoClient

from learn import functions
from loader import PageParser
from race import Race
from conf import mongo

baseUrl = 'http://race.netkeiba.com/?pid=race_list_sub&id=c'
db = mongo.getMongoClient()
db.drop_collection('card_data')
db.drop_collection('card_urls')
db.drop_collection('errors')

def loadCard(target):
    parser = PageParser()
    url = baseUrl + target
    html = parser.parse(url)
    for a in parser.parsed.find_all(href=re.compile("^/\?pid")):
        collect = db.card_urls
        if db.card_urls.find({'url': a.get('href')}).count() == 0:
            collect.insert({'url': a.get('href'), 'date': target})
    return db.card_urls.find({'date':target})

##
# 出馬表のパースロジック
#
def parseLogic(url):
    try :
        if 'url' not in url:
            return
        elif 'mode' in url['url']:
            url['url'] = re.sub(r'&mode=.*', "", url['url'])
            url['url'] = re.sub(r'pid=race', "pid=race_old", url['url'])
        print 'http://race.netkeiba.com' + url["url"]
        parser = PageParser()
        html = parser.parse('http://race.netkeiba.com' + url["url"])
        if html != False and len(parser.getParsed().select(".race_place a.active")) != 0:
            corse = parser.getParsed().select(".race_place a.active")[0].string
            if len(parser.getParsed().select(".race_num a.active")) == 0:
                db = mongo.getMongoClient()
                db.errors.insert(url)
                return
            race_num = parser.getParsed().select(".race_num a.active")[0].string
            race_info = parser.getParsed().select(".racedata.fc span")[0].string
            race_name = parser.getParsed().title.string.split(unicode(' ', 'utf-8'))[3].strip().split('(')[0]
            date = parser.getParsed().title.string.split(unicode(' ', 'utf-8'))[0].strip()
            date = date.split(unicode('|', 'utf-8'))[0].strip()
            race = Race(race_name, race_info, race_num, corse, date, 'http://race.netkeiba.com' + url["url"])
            for td in parser.getParsed().select('.bml1'):
                elms = td.select('td')
                #elms = parser.getParsed().select('.bml1 td')
                #枠番
                gate_num = elms[0].string
                #馬番
                hose_num = elms[1].string
                #馬名
                hose_name = elms[3].select('a')[0].string
                #馬齢
                hose_age = elms[4].string
                #斤量
                weight = float(elms[5].string)
                #騎手
                jockey = elms[6].string
                #厩舎
                trainer = elms[7].string
                json_data = race.dump()
                if gate_num.isdigit() == False:
                    print gate_num
                    continue
                if hose_num.isdigit() == False:
                    print hose_num
                    continue
                json_data['gate_num'] = int(gate_num)
                json_data['hose_num'] = int(hose_num)
                json_data['hose_name'] = hose_name
                json_data['hose_age'] = hose_age
                json_data['basis_weight'] = weight
                json_data['jockey'] = jockey
                json_data['trainer'] = trainer
                raceInfo(json_data)
                db = mongo.getMongoClient()
                collect = db.card_data
                collect.insert(json_data)
    except Exception as e:
        print '=== エラー内容 ==='
        print 'type:' + str(type(e))
        print 'args:' + str(e.args)
        print 'message:' + e.message
        print 'e自身:' + str(e)
    finally:
        pass

def raceInfo(json):
    race_info = json["race_info"]
    datas = []
    for value in race_info.split(unicode('/', 'utf-8')):
        for dataValue in value.strip().split(unicode(' : ', 'utf-8')):
            datas.append(dataValue)
    dna = datas[0]
    pattern = re.compile('([0-9]+)')
    match = pattern.search(dna)
    json['corse_data'] = int(match.group(1))
    if '芝' in datas[0].encode('utf-8'):
        json['quality'] = '芝'
    else:
        json['quality'] = 'ダ'
    json['start'] = datas[3]

def updateRaces():
    db = mongo.getMongoClient()
    races = db.card_data
    for item in races.find({'race_date': {'$type': 2}}):
        race_date = item["race_date"]
        # newDate = dt.strptime(unicode(race_date, 'utf-8'),'%Y年%m月%d日')
        newDate = datetime.datetime.strptime(race_date.encode('utf-8'),'%Y/%m/%d')
        item['race_date'] = newDate
        item['date'] = newDate.strftime('%Y/%m/%d')
        races.save(item)

def leaning():
    db = mongo.getMongoClient()
    collect = db.card_data
    p = Pool(multiprocessing.cpu_count())
    p.map(functions.lean, collect.find())

def load(target):
    dic = loadCard(target)
    p = Pool(multiprocessing.cpu_count())
    p.map(parseLogic, dic)
    db = mongo.getMongoClient()
    errors = db.errors.find()
    while db.errors.count() != 0:
        db.drop_collection('errors')
        p = Pool(multiprocessing.cpu_count())
        p.map(parseLogic, errors)
        errors = db.errors.find()
if __name__ == "__main__":
    for time in range(7):
        target = datetime.date.today() + datetime.timedelta(days=(time))
        load(target.strftime("%m%d"))
    updateRaces()
    leaning()
    print '正常終了'
