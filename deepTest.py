# coding: UTF-8
import datetime
import os
import simplejson
from loader import PageParser
from race import Race
from multiprocessing import Pool
import multiprocessing
from pymongo import MongoClient
import traceback
import re
from learn import functions
import psycopg2
import sys
from psycopg2.extensions import adapt, register_adapter, AsIs
from hose import Hose

print sys.getdefaultencoding()
hostName = "host=hosesample.c6vhn4foie7p.ap-northeast-1.redshift.amazonaws.com"
databaseName = "hosesample"
portNo = "5439"
userName = "admin"
password = "Jk9t5ybma"

# conn = psycopg2.connect(
#     host=hostName,
#     database=databaseName,
#     port=portNo,
#     user=userName,
#     password=password
# )

def getClient() :
    connect = MongoClient('ec2-52-198-180-103.ap-northeast-1.compute.amazonaws.com', 27017)
    connect.hose_race.authenticate('root', 'pass')
    return connect.hose_race

def  adapt_hose(hose):
    result = AsIs("{gate_num},{trainer},{weight},{hose_num},{weather},{through},{hose_name},{owner},{last_rap},{hose_age},{sub},{quality},{traning_time},{start},{race_id},{race_name},{race_corse},{race_date},{fav},{corse_data},{race_number},{jockey},{condition},{shissu},{basis_weight},{race_info},{prize},{odds},{comment},{time},{order},".format(gate_num = adapt(hose.gate_num),
                                                                                                                                                                                                                                                                                                                                                           trainer = adapt(hose.trainer),
                                                                                                                                                                                                                                                                                                                                                           weight = adapt(hose.weight),
                                                                                                                                                                                                                                                                                                                                                           hose_num = adapt(hose.hose_num),
                                                                                                                                                                                                                                                                                                                                                           weather = adapt(hose.weather),
                                                                                                                                                                                                                                                                                                                                                           through = adapt(hose.through),
                                                                                                                                                                                                                                                                                                                                                           hose_name = adapt(hose.hose_name),
                                                                                                                                                                                                                                                                                                                                                           owner = adapt(hose.owner),
                                                                                                                                                                                                                                                                                                                                                           last_rap = adapt(hose.last_rap),
                                                                                                                                                                                                                                                                                                                                                           hose_age = adapt(hose.hose_age),
                                                                                                                                                                                                                                                                                                                                                           sub = adapt(hose.sub),
                                                                                                                                                                                                                                                                                                                                                           quality = adapt(hose.quality),
                                                                                                                                                                                                                                                                                                                                                           traning_time = adapt(hose.traning_time),
                                                                                                                                                                                                                                                                                                                                                           start = adapt(hose.start),
                                                                                                                                                                                                                                                                                                                                                           race_id = adapt(hose.race_id),
                                                                                                                                                                                                                                                                                                                                                           race_name = adapt(hose.race_name),
                                                                                                                                                                                                                                                                                                                                                           race_corse = adapt(hose.race_corse),
                                                                                                                                                                                                                                                                                                                                                           race_date = adapt(hose.race_date),
                                                                                                                                                                                                                                                                                                                                                           fav = adapt(hose.fav),
                                                                                                                                                                                                                                                                                                                                                           corse_data = adapt(hose.corse_data),
                                                                                                                                                                                                                                                                                                                                                           race_number = adapt(hose.race_number),
                                                                                                                                                                                                                                                                                                                                                           jockey = adapt(hose.jockey),
                                                                                                                                                                                                                                                                                                                                                           condition = adapt(hose.condition),
                                                                                                                                                                                                                                                                                                                                                           shissu = adapt(hose.shissu),
                                                                                                                                                                                                                                                                                                                                                           basis_weight = adapt(hose.basis_weight),
                                                                                                                                                                                                                                                                                                                                                           race_info = adapt(hose.race_info),
                                                                                                                                                                                                                                                                                                                                                           prize = adapt(hose.prize),
                                                                                                                                                                                                                                                                                                                                                           odds = adapt(hose.odds),
                                                                                                                                                                                                                                                                                                                                                           comment = adapt(hose.comment),
                                                                                                                                                                                                                                                                                                                                                           time = adapt(hose.time),
                                                                                                                                                                                                                                                                                                                                                           order = str(hose.order_num)))
    print result
    return result + "," + adapt(hose.order_num)
connection = psycopg2.connect("host=hosesample.c6vhn4foie7p.ap-northeast-1.redshift.amazonaws.com port=5439 dbname=hosesample user=admin password=Jk9t5ybma")
cursor = connection.cursor()
# cursor.execute("create table hoses (_id text , gate_num text , trainer text , weight text , hose_num text , weather text , through text , hose_name text , owner text , last_rap text , hose_age text , sub text , quality text , traning_time text , start text , race_id text , race_name text , race_corse text , race_date text , fav text , corse_data text , race_number text , jockey text , condition text , shissu text , basis_weight text , race_info text , prize text , odds text , comment text , time text , _order text)  ")
# cursor.execute("commit")
rows = ['gate_num','trainer','weight','hose_num','weather','through','hose_name','owner','last_rap','hose_age','sub','quality','traning_time','start','race_id','race_name','race_corse','race_date','fav','corse_data','race_number','jockey','condition','shissu','basis_weight','race_info','prize','odds','comment','time','order']
sql_base = "INSERT INTO hose_history (gate_num,trainer,weight,hose_num,weather,through,hose_name,owner,last_rap,hose_age,sub,quality,traning_time,start,race_id,race_name,race_corse,race_date,fav,corse_data,race_number,jockey,condition,shissu,basis_weight,race_info,prize,odds,comment,time,order_num) VALUES (%s)"


db = getClient()
for hose in db.hoses.find():
    cursor = connection.cursor()
    register_adapter(Hose, adapt_hose)
    hoseObj = Hose(hose)
    cursor.execute(sql_base, [hoseObj])
    connection.commit()

