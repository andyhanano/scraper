# coding: UTF-8

import re
from pymongo import MongoClient

from loader import PageParser

errors = []
def getClient() :
    connect = MongoClient('ec2-52-198-180-103.ap-northeast-1.compute.amazonaws.com', 27017)
    connect.hose_race.authenticate('root', 'pass')
    return connect.hose_race


def updateRaces():
    db = getClient()
    races = db.races
    for item in races.find():
        data = {}
        race_info = item["race_info"]
        datas = []
        for value in race_info.split(unicode('/', 'utf-8')):
            for dataValue in value.strip().split(unicode(' : ', 'utf-8')):
                datas.append(dataValue)
        dna = datas[0]
        pattern = re.compile('([0-9]+)')
        match = pattern.search(dna)

        item['corse_data'] = int(match.group(1))
        item['weather'] = datas[2]
        item['quality'] = datas[3]
        item['condition'] = datas[4]
        item['start'] = datas[6]
        races.save(item)


def updateHoseRaces():
    db = getClient()
    races = db.hoses
    for item in races.find():
        data = {}
        race_info = item["race_info"]
        item['basis_weight'] = float(item['basis_weight'])
        datas = []
        for value in race_info.split(unicode('/', 'utf-8')):
            for dataValue in value.strip().split(unicode(' : ', 'utf-8')):
                datas.append(dataValue)
        dna = datas[0]
        pattern = re.compile('([0-9]+)')
        match = pattern.search(dna)
        if match == None:
            print dna
        item['corse_data'] = int(match.group(1))
        item['weather'] = datas[2]
        item['quality'] = datas[3]
        item['condition'] = datas[4]
        item['start'] = datas[6]
        races.save(item)

def getGrade(item):
    db = getClient()
    races = db.races
    url = item["url"]

    parser = PageParser()
    result = parser.parse(url)
    if result == False:
        errors.append(item)
        print url
        return
    racedata = parser.parsed.h1
    dna = str(racedata)
    pattern = re.compile('\(([^\(|\)]+)\)')
    match = pattern.search(dna)
    if match == None:
        item["grade"] = ""
        item["score_weight"] = 0.3
    else:
        item["grade"] = match.group(1)
        if "G1" in item["grade"]:
            item["score_weight"] = 1
        elif "G2" in item["grade"]:
            item["score_weight"] = 0.8
        elif "G3" in item["grade"]:
            item["score_weight"] = 0.6
    races.save(item)

updateRaces()
updateHoseRaces()
# connect = MongoClient('localhost', 27017)
# db = connect.hose_race
# items = db.races.find({"grade":  None})
# p = Pool(20)
# p.map(getGrade, items)
#
#
# while len(errors) != 0:
#     _errors = errors
#     errors = []
#     p = Pool(20)
#     p.map(getGrade, _errors)
