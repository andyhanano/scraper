# coding: UTF-8
import codecs
import pymongo
from pymongo import MongoClient

baseUrl = 'http://race.netkeiba.com/?pid=race_list_sub&id=c'
connect = MongoClient('localhost', 27017)
db = connect.hose_race
#db.drop_collection('card_odds')
connect.close()


if __name__ == "__main__":
    dic = {}
    data = db.card_odds.find({"race_name": "中山金杯"}).sort("time", pymongo.DESCENDING)
    for item in data:
        time = item["time"]
        time = time.strftime('%Y/%m/%d %H:%M:%S')
        if time not in dic:
            dic[time] = {}
        hose = item["hose_name"].encode("utf-8")
        dic[time][hose] = item["odds"]
    f = codecs.open("./races/金杯.csv", "w", "UTF-8")
    header = []
    lines = []
    for key in dic:
        if len(header) == 0:
            # header.append(" ".encode("utf-8"))
            for hose in dic[key]:
                header.append(hose)
            headerLine = ",".join(header)
            lines.append(headerLine)
        line = []
        line.append(key)
        for hose in header:
            if hose in dic[key]:
                line.append(dic[key][hose].encode("utf-8"))
            elif hose not in dic[key]:
                line.append("0".encode("utf-8"))
        #f.write(",".join(line))
        lines.append(",".join(line))
    print "\n".join(lines)
    f.write("\n".join(lines))
    f.close();
    print '正常終了'