# -*- coding: utf-8 -*-

from datetime import datetime as dt
from pymongo import MongoClient


def getClient() :
    connect = MongoClient('ec2-52-198-180-103.ap-northeast-1.compute.amazonaws.com', 27017)
    connect.hose_race.authenticate('root', 'pass')
    return connect.hose_race

def updateRaces():
    db = getClient()
    races = db.races
    for item in races.find({'race_date': {'$type': 2}}):
        race_date = item["race_date"]
        # newDate = dt.strptime(unicode(race_date, 'utf-8'),'%Y年%m月%d日')
        newDate = dt.strptime(race_date.encode('utf-8'),'%Y年%m月%d日')
        item['race_date'] = newDate
        races.save(item)


def updateHoseRaces():
    db = getClient()
    races = db.hoses
    for item in races.find({'race_date': {'$type': 2}}):
        race_date = item["race_date"]
        newDate = dt.strptime(race_date.encode('utf-8'),'%Y年%m月%d日')
        item['race_date'] = newDate
        print newDate
        races.save(item)
def cardDate():
    db = getClient()
    races = db.card_data
    for item in races.find():
        race_date = item["race_date"]
        item['date'] = race_date.strftime('%Y/%m/%d')
        races.save(item)
# updateRaces()
# updateHoseRaces(
cardDate()